const fromStream = (stream, callback) => {
  const objects = [];
  let buffer = '';

  stream.on('data', chunk => {
    // assume newline character is the delimiter,
    // concat the current chunk with the buffer.
    const lines = `${buffer}${chunk.toString()}`.split('\n');

    // reset the buffer
    buffer = '';

    lines.forEach(line => {
      try {
        objects.push(JSON.parse(line));
      } catch (e) {
        buffer = line;
      }
    });
  });

  const closeOrEnd = () => {
    if (buffer !== '') {
      return callback(new Error('incomplete stream'), objects);
    }
    callback(null, objects);
  };

  stream.on('close', () => closeOrEnd());

  stream.on('end', () => closeOrEnd());

  // on error pass the error with currently processed objects
  stream.on('error', err => {
    callback(err, objects);
  });
};

module.exports = { fromStream };
