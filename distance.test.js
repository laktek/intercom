const assert = require('assert').strict;
const distance = require('./distance');

// let's use some well-known values to test the Great Circle Distance algorithm

// For Intercom office co-ords it must be 0
assert.equal(distance([53.339428, -6.257664], [53.339428, -6.257664]), 0);

// JFK - LAX (~4000km)
assert.equal(
  distance([40.6413, 73.7781], [34.0522, 118.2437]),
  3955.4500509057966
);

// MEL - SYD (~700km)
assert.equal(
  distance([37.669, 144.841], [33.8688, 151.2093]),
  712.9165022557129
);
