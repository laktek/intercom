const assert = require('assert').strict;
const streams = require('./streams');
const { Readable } = require('stream');

// TODO: This test relies on the existence of customers.txt file.
// We should mock the file system calls for the purpose of unit tests.
assert.ok(streams.createFS('customers.txt') instanceof Readable);
