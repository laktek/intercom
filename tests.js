// Require all test files here

require('./distance.test');
require('./filters.test');
require('./parse_json.test');
require('./sorts.test');
require('./streams.test');
