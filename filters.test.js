const assert = require('assert').strict;
const filters = require('./filters');
const distanceFn = require('./distance');

// it doesn't include items without latitude/longtiude
assert.deepEqual(
  [{}, { latitude: 0 }, { longitude: 0 }].filter(
    filters.byDistanceToBase(distanceFn, [0, 0], 100)
  ),
  []
);

// it only includes an item when its less than max distance
assert.deepEqual(
  [{ latitude: 10, longitude: 10 }].filter(
    filters.byDistanceToBase(() => 50, [0, 0], 100)
  ),
  [{ latitude: 10, longitude: 10 }]
);

// it only includes an item when its equal to the max distance
assert.deepEqual(
  [{ latitude: 50, longitude: 50 }].filter(
    filters.byDistanceToBase(() => 100, [0, 0], 100)
  ),
  [{ latitude: 50, longitude: 50 }]
);

// it does not include an item when its above the max distance
assert.deepEqual(
  [{ latitude: 10, longitude: 50 }].filter(
    filters.byDistanceToBase(() => 150, [0, 0], 100)
  ),
  []
);
