const assert = require('assert').strict;
const { Readable } = require('stream');

const parseJSON = require('./parse_json');

// Empty objects stream
const s1 = new Readable();
s1.push('{}');
s1.push('{}');
s1.push(null);

parseJSON.fromStream(s1, (err, objects) => {
  assert.equal(err, null);
  assert.deepEqual(objects, [{}, {}]);
});

// stream with partial chunks
const s2 = new Readable();
s2.push('{ "name": "foo" }\n{ "name');
s2.push('": "bar" }');
s2.push(null);

parseJSON.fromStream(s2, (err, objects) => {
  assert.equal(err, null);
  assert.deepEqual(objects, [{ name: 'foo' }, { name: 'bar' }]);
});

// stream with invalid JSON
const s3 = new Readable();
s3.push('{ "name": "foo" }\n{ "name');
s3.push('": "bar" }\n{ "n');
s3.push(null);

parseJSON.fromStream(s3, (err, objects) => {
  assert.throws(() => assert.ifError(err), Error);
  assert.deepEqual(objects, [{ name: 'foo' }, { name: 'bar' }]);
});

// stream with multiple buffered chunks
const s4 = new Readable();
s4.push('{ "name": "foo",');
s4.push('"user_id": 15,');
s4.push('"latitude": ');
s4.push('"54.0894797" }\n');
s4.push(null);

parseJSON.fromStream(s4, (err, objects) => {
  assert.equal(err, null);
  assert.deepEqual(objects, [
    { name: 'foo', user_id: 15, latitude: '54.0894797' },
  ]);
});
