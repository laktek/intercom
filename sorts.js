// This module will contain all possible sorting methods for the list.

const byUserID = (a, b) => {
  if (!a || !a.user_id) return;
  return a.user_id - b.user_id;
};

module.exports = { byUserID };
