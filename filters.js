// This module will contain all possible filtering methods for the list.

// byDistanceToBase filters items that are less than or equal to given max distance (in Km),
// from the base point (lat,lon). It takes a function to calculate the distance as the first arg.
const byDistanceToBase = (distanceFn, baseCoords, maxDistance) => {
  if (typeof distanceFn !== 'function') {
    throw new Error('distanceFn must be a function');
  }

  return member => {
    if (!member.latitude || !member.longitude) {
      return;
    }

    const curCoords = [
      Number.parseFloat(member.latitude),
      Number.parseFloat(member.longitude),
    ];

    return distanceFn(baseCoords, curCoords) <= maxDistance;
  };
};

module.exports = { byDistanceToBase };
