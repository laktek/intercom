const streams = require('./streams');
const parseJSON = require('./parse_json');
const filters = require('./filters');
const sorts = require('./sorts');
const distanceFn = require('./distance');

// Read data from given file path.
// Defaults to customer.txt.
// Users can override the path by providing command-line arguments.
// Eg. npm run app -- customers2.txt
const path =
  process.argv[2] && process.argv[2] !== '' ? process.argv[2] : 'customers.txt';

parseJSON.fromStream(streams.createFS(path), (err, customers) => {
  if (err) {
    console.error(
      `Error occurred reading ${path} file.\nRecords parsed: ${
        customers.length
      }\n${err}`
    );
    // Suggestion: Shall we process partially parsed records? This will keep the program resilient.
    return;
  }

  // TODO: make them configurable
  // We can proabably read them from a config file.
  // Decided it'd be out of scope for this excercise.
  const maxDistance = 100; // in kms
  const baseCoords = [53.339428, -6.257664];

  customers
    .filter(filters.byDistanceToBase(distanceFn, baseCoords, maxDistance))
    .sort(sorts.byUserID)
    .forEach(customer => {
      console.info(`Name: ${customer.name}\nUser ID: ${customer.user_id}\n`);
    });
});
