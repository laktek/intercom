const assert = require('assert').strict;
const sorts = require('./sorts');

// empty array
assert.deepEqual([].sort(sorts.byUserID), []);

// array with null and undefined
assert.deepEqual([null, { user_id: 1 }, undefined].sort(sorts.byUserID), [
  null,
  { user_id: 1 },
  undefined,
]);

// array with user_id is null or undefined
assert.deepEqual(
  [
    { user_id: null },
    { user_id: undefined },
    { user_id: 2 },
    { user_id: 1 },
  ].sort(sorts.byUserID),
  [{ user_id: null }, { user_id: undefined }, { user_id: 1 }, { user_id: 2 }]
);

// array with user_ids (3 items)
assert.deepEqual(
  [{ user_id: 2 }, { user_id: 1 }, { user_id: 3 }].sort(sorts.byUserID),
  [{ user_id: 1 }, { user_id: 2 }, { user_id: 3 }]
);

// array with random user_ids (more than 3 items)
assert.deepEqual(
  [
    { user_id: 100 },
    { user_id: 12 },
    { user_id: 43 },
    { user_id: 689 },
    { user_id: 71888 },
    { user_id: 25 },
  ].sort(sorts.byUserID),
  [
    { user_id: 12 },
    { user_id: 25 },
    { user_id: 43 },
    { user_id: 100 },
    { user_id: 689 },
    { user_id: 71888 },
  ]
);

// array with min/max values
assert.deepEqual(
  [
    { user_id: 100 },
    { user_id: Number.MIN_VALUE },
    { user_id: Number.MAX_VALUE },
  ].sort(sorts.byUserID),
  [
    { user_id: Number.MIN_VALUE },
    { user_id: 100 },
    { user_id: Number.MAX_VALUE },
  ]
);
