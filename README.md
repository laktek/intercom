# Intercom - Take home work

### Running the program

* Install Node.JS 10.x
* To run the program, either:

    node index.js

  or

    npm run app

* By default, it reads the `customers.txt` included in the root as the default dataset.
* If you want to provide a different dataset, provde the path to it as an argument:

    node index.js /path/to/customers.txt

  or

    npm run app -- /path/to/customers.txt

### Running tests

* You can run the unit tests by calling:

    npm run tests

### Decisions

* For the ease of reviewing code, I decided not to use any npm dependencies.
* Tests were written asserts module from the standard library.
* I did away with a naive implementation of a JSON stream parser. For production, I would use an established library like stream-json.
* These days I prefer to write async code using async/await. However, for this task I used callbacks for simplicity (did't wanna setup transpilers).
