// Approximation for mean earth radius
// based on https://en.wikipedia.org/wiki/Great-circle_distance#Radius_for_spherical_Earth
const meanEarthRadius = 6371;

const convertToRadians = val => (val * Math.PI) / 180;

const distance = (pointA, pointB) => {
  const [latA, lonA] = pointA.map(convertToRadians);
  const [latB, lonB] = pointB.map(convertToRadians);

  const lonDelta = lonB - lonA;
  var a =
    Math.pow(Math.cos(latB) * Math.sin(lonDelta), 2) +
    Math.pow(
      Math.cos(latA) * Math.sin(latB) -
        Math.sin(latA) * Math.cos(latB) * Math.cos(lonDelta),
      2
    );
  var b =
    Math.sin(latA) * Math.sin(latB) +
    Math.cos(latA) * Math.cos(latB) * Math.cos(lonDelta);
  var angle = Math.atan2(Math.sqrt(a), b);

  return angle * meanEarthRadius;
};

module.exports = distance;
