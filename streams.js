const fs = require('fs');

const createFS = path => {
  // for the purpose of this excercise we'll limit the reads to 1Mb in 1Kb chunks.
  return fs.createReadStream(path, {
    encoding: 'utf8',
    start: 0,
    end: 1024 * 1024,
    highWaterMark: 1024,
  });
};

module.exports = { createFS };
